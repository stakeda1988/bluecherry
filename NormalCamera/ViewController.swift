//
//  ViewController.swift
//  NormalCamera
//
//  Created by SHOKI TAKEDA on 3/20/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit
import Social
import Foundation
import CoreImage

extension String {
    
    /// String -> NSString に変換する
    func to_ns() -> NSString {
        return (self as NSString)
    }
    
    func substringFromIndex(index: Int) -> String {
        return to_ns().substringFromIndex(index)
    }
    
    func substringToIndex(index: Int) -> String {
        return to_ns().substringToIndex(index)
    }
    
    func substringWithRange(range: NSRange) -> String {
        return to_ns().substringWithRange(range)
    }
    
    var lastPathComponent: String {
        return to_ns().lastPathComponent
    }
    
    var pathExtension: String {
        return to_ns().pathExtension
    }
    
    var stringByDeletingLastPathComponent: String {
        return to_ns().stringByDeletingLastPathComponent
    }
    
    var stringByDeletingPathExtension: String {
        return to_ns().stringByDeletingPathExtension
    }
    
    var pathComponents: [String] {
        return to_ns().pathComponents
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        return to_ns().stringByAppendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        return to_ns().stringByAppendingPathExtension(ext)
    }
    
}

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NADViewDelegate {
    var upCameraBool:Bool = true
    var globalImgWidth:CGFloat = 0
    var globalImgHeight:CGFloat = 0
    var globalImg:UIImage?
    var globalRatio:CGFloat = 0
    var postImg:UIImage?
    var originalImg:UIImage?
    
    var shipImageView : UIImageView!
    var myImageView1:UIImageView!
    var myImageView2:UIImageView!
    
    private var nadView: NADView!
    var adTimer:NSTimer!
    
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var topImgHeight: NSLayoutConstraint!
    @IBOutlet weak var topImgWidth: NSLayoutConstraint!
    @IBOutlet weak var instagramBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var cameraImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                          spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        twitterBtn.backgroundColor = UIColor.clearColor()
        twitterBtn.setImage(UIImage(named: "twitter-big.jpg"), forState: .Normal)
        twitterBtn.imageView!.contentMode = .ScaleAspectFit
        
        fbBtn.backgroundColor = UIColor.clearColor()
        fbBtn.setImage(UIImage(named: "facebook-big.jpg"), forState: .Normal)
        fbBtn.imageView!.contentMode = .ScaleAspectFit
        
        instagramBtn.backgroundColor = UIColor.clearColor()
        instagramBtn.setImage(UIImage(named: "instagram"), forState: .Normal)
        instagramBtn.imageView!.contentMode = .ScaleAspectFit
        
        saveBtn.backgroundColor = UIColor.clearColor()
        saveBtn.setImage(UIImage(named: "save-big"), forState: .Normal)
        saveBtn.imageView!.contentMode = .ScaleAspectFit
        
        cameraBtn.backgroundColor = UIColor.clearColor()
        cameraBtn.setImage(UIImage(named: "camera-big"), forState: .Normal)
        cameraBtn.imageView!.contentMode = .ScaleAspectFit
        
        selectBtn.backgroundColor = UIColor.clearColor()
        selectBtn.setImage(UIImage(named: "album"), forState: .Normal)
        selectBtn.imageView!.contentMode = .ScaleAspectFit
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if upCameraBool == false && globalImg != nil {
            if shipImageView != nil {
                shipImageView.removeFromSuperview()
            }
            if myImageView1 != nil {
                myImageView1.removeFromSuperview()
            }
            if myImageView2 != nil {
                myImageView2.removeFromSuperview()
            }
            let picName:String = "nkusakina.jpg"
            let uiImg:UIImage = UIImage(named:picName)!
            let data = UIImagePNGRepresentation(uiImg)
            self.shipImageView = UIImageView(image:UIImage(data:data!))
            globalRatio = globalImg!.size.width / globalImg!.size.height
            globalImgHeight = self.view.frame.height - 180
            self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
            self.view.addSubview(self.shipImageView)
            
            let myImage : UIImage = UIImage.ResizeÜIImage(globalImg!, width: self.view.frame.width, height: self.view.frame.height)
            let options : NSDictionary = NSDictionary(object: CIDetectorAccuracyHigh, forKey: CIDetectorAccuracy)
            let detector : CIDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options as! [String : AnyObject])
            let faces : NSArray = detector.featuresInImage(CIImage(image: myImage)!)
            var transform : CGAffineTransform = CGAffineTransformMakeScale(1, -1)
            transform = CGAffineTransformTranslate(transform, 0, -self.view.frame.height)
            var globalLeftEyeX:CGFloat = 0
            var globalLeftEyeY:CGFloat = 0
            var globalRightEyeX:CGFloat = 0
            var globalRightEyeY:CGFloat = 0
            var globalFaceMinX:CGFloat = 0
            var globalFaceMaxX:CGFloat = 0
            var globalFaceMinY:CGFloat = 0
            var globalFaceMaxY:CGFloat = 0
            var faceCounter:Int = 0
            for feature in faces {
                if faceCounter == 0 {
                    globalLeftEyeX = feature.leftEyePosition.x
                    globalLeftEyeY = feature.leftEyePosition.y
                    globalRightEyeX = feature.rightEyePosition.x
                    globalRightEyeY = feature.rightEyePosition.y
                    globalFaceMinX = feature.bounds.minX
                    globalFaceMaxX = feature.bounds.maxX
                    globalFaceMinY = feature.bounds.minY
                    globalFaceMaxY = feature.bounds.maxY
                }
                faceCounter++
            }
            self.shipImageView.image = globalImg
            let faceWidth = globalFaceMaxX-globalFaceMinX
            let myInputImage1 = CIImage(image: UIImage(named: "cheek")!)
            myImageView1 = UIImageView(frame: CGRectMake(globalLeftEyeX-faceWidth/10, self.view.frame.height-globalLeftEyeY*(globalImgHeight/self.view.frame.height)-(self.view.frame.height/2 - globalImgHeight/2), faceWidth/5, faceWidth/5))
            myImageView1.image = UIImage(CIImage: myInputImage1!)
            self.view.addSubview(myImageView1)
            self.view.bringSubviewToFront(myImageView1)
            
            let myInputImage2 = CIImage(image: UIImage(named: "cheek")!)
            myImageView2 = UIImageView(frame: CGRectMake(globalRightEyeX-faceWidth/10, self.view.frame.height-globalRightEyeY*(globalImgHeight/self.view.frame.height)-(self.view.frame.height/2 - globalImgHeight/2), faceWidth/5, faceWidth/5))
            myImageView2.image = UIImage(CIImage: myInputImage2!)
            self.view.addSubview(myImageView2)
            self.view.bringSubviewToFront(myImageView2)
            
            let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
            UIGraphicsBeginImageContext(self.view.bounds.size)
            self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
            let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
            let cropImage = UIImage(CGImage: cropRef!)
            
            originalImg = cropImage
            postImg = cropImage
        }
        cameraImgView.hidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if upCameraBool == true {
            let picker:UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(picker, animated: true, completion: nil)
        }
        upCameraBool = false
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let stampViews = cameraImgView.subviews
        for stampView in stampViews {
            stampView.removeFromSuperview()
        }
        globalImg = image
        originalImg = image
        cameraImgView.hidden = false
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func twitter(sender: AnyObject) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Twitter?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let title: String = "#UlzzangCamera"
            controller.setInitialText(title)
            controller.addImage(self.postImg)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func facebook(sender: AnyObject) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Facebok?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let title: String = "#UlzzangCamera"
            controller.setInitialText(title)
            controller.addImage(self.postImg)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    var documentController:UIDocumentInteractionController?
    @IBAction func instagram(sender: AnyObject) {
        let imageData = UIImageJPEGRepresentation(postImg!, 1.0)
        let temporaryDirectory = NSTemporaryDirectory() as String
        let temporaryImagePath = temporaryDirectory.stringByAppendingPathComponent("postImg.igo")
        let boolValue = imageData!.writeToFile(temporaryImagePath, atomically: true)
        let fileURL = NSURL(fileURLWithPath: temporaryImagePath)
        documentController = UIDocumentInteractionController(URL: fileURL)
        documentController!.UTI = "com.instagram.exclusivegram"
        documentController!.presentOpenInMenuFromRect(
            self.view.frame,
            inView: self.view,
            animated: true
        )
    }
    
    @IBAction func reserve(sender: UIButton) {
        if postImg != nil {
            UIImageWriteToSavedPhotosAlbum(postImg!, self, nil, nil)
            let alertController = UIAlertController(title: "Complete", message: "Successfully Saved.", preferredStyle: .Alert)
            let otherAction = UIAlertAction(title: "OK", style: .Default) {
                action in self.dismissViewControllerAnimated(true, completion: nil)
            }
            alertController.addAction(otherAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func takeAgain(sender: UIButton) {
        let picker:UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func selectImg(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePickerController.allowsEditing = false
        imagePickerController.delegate = self
        self.presentViewController(imagePickerController,animated:true ,completion:nil)
    }
    
    @IBAction func btn1(sender: UIButton) {
        if shipImageView != nil {
            shipImageView.removeFromSuperview()
        }
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectInstant")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.shipImageView = UIImageView(image:image2!)
        self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        self.view.addSubview(self.shipImageView)
        self.view.bringSubviewToFront(myImageView1)
        self.view.bringSubviewToFront(myImageView2)
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn2(sender: UIButton) {
        if shipImageView != nil {
            shipImageView.removeFromSuperview()
        }
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectChrome")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.shipImageView = UIImageView(image:image2!)
        self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        self.view.addSubview(self.shipImageView)
        self.view.bringSubviewToFront(myImageView1)
        self.view.bringSubviewToFront(myImageView2)
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn3(sender: UIButton) {
        if shipImageView != nil {
            shipImageView.removeFromSuperview()
        }
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIColorCrossPolynomial")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let r: [CGFloat] = [1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        let g: [CGFloat] = [0.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        let b: [CGFloat] = [0.0, 0.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        ciFilter.setValue(CIVector(values: r, count: 10), forKey: "inputRedCoefficients")
        ciFilter.setValue(CIVector(values: g, count: 10), forKey: "inputGreenCoefficients")
        ciFilter.setValue(CIVector(values: b, count: 10), forKey: "inputBlueCoefficients")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.shipImageView = UIImageView(image:image2!)
        self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        self.view.addSubview(self.shipImageView)
        self.view.bringSubviewToFront(myImageView1)
        self.view.bringSubviewToFront(myImageView2)
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn4(sender: UIButton) {
        if shipImageView != nil {
            shipImageView.removeFromSuperview()
        }
        //        let ciImage:CIImage = CIImage(image:originalImg!)!
        //        let ciFilter:CIFilter = CIFilter(name: "CIToneCurve" )!
        //        ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
        //        ciFilter.setValue(CIVector(x: 0.0, y: 0.0), forKey: "inputPoint0")
        //        ciFilter.setValue(CIVector(x: 0.25, y: 0.1), forKey: "inputPoint1")
        //        ciFilter.setValue(CIVector(x: 0.5, y: 0.5), forKey: "inputPoint2")
        //        ciFilter.setValue(CIVector(x: 0.75, y: 0.9), forKey: "inputPoint3")
        //        ciFilter.setValue(CIVector(x: 1.0, y: 1.0), forKey: "inputPoint4")
        //        let ciContext:CIContext = CIContext(options: nil)
        //        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        //        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.shipImageView = UIImageView(image:originalImg!)
        self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        self.view.addSubview(self.shipImageView)
        self.view.bringSubviewToFront(myImageView1)
        self.view.bringSubviewToFront(myImageView2)
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn5(sender: UIButton) {
        if shipImageView != nil {
            shipImageView.removeFromSuperview()
        }
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectProcess")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.shipImageView = UIImageView(image:image2!)
        self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        self.view.addSubview(self.shipImageView)
        self.view.bringSubviewToFront(myImageView1)
        self.view.bringSubviewToFront(myImageView2)
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    @IBAction func btn6(sender: UIButton) {
        if shipImageView != nil {
            shipImageView.removeFromSuperview()
        }
        let ciImage:CIImage = CIImage(image:originalImg!)!
        let ciFilter:CIFilter = CIFilter(name: "CIPhotoEffectTransfer")!
        ciFilter.setValue(ciImage, forKey: "inputImage")
        let ciContext:CIContext = CIContext(options: nil)
        let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
        let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
        
        self.shipImageView = UIImageView(image:image2!)
        self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        self.view.addSubview(self.shipImageView)
        self.view.bringSubviewToFront(myImageView1)
        self.view.bringSubviewToFront(myImageView2)
        let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
        
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef!)
        
        postImg = cropImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                          spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                          spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}

